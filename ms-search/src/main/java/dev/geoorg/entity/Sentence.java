package dev.geoorg.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.SASI;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.UUID;

@Table("ms_sentence")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Sentence {
    @PrimaryKey
    private UUID id;

    @Indexed
    @SASI(indexMode = SASI.IndexMode.CONTAINS)
    @SASI.StandardAnalyzed
    private String sentence;
}
