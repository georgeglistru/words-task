package dev.geoorg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsSearchApplication {
    public static void main(String[] args) {
        SpringApplication.run(MsSearchApplication.class,args);
    }
}
