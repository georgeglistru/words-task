package dev.geoorg.service;

import dev.geoorg.entity.Sentence;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface SentenceService {
    /**
     *  Method for finding word in db by substring
     * @param find - searched substring
     * @return Flux<Sentence> lots of sentences
     */
    Flux<Sentence> findByWord(Mono<String> find);
}
