package dev.geoorg.service;

import dev.geoorg.entity.Sentence;
import dev.geoorg.repo.ReactiveSentenceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class SentenceServiceImpl implements SentenceService {

    @Autowired
    ReactiveSentenceRepo reactiveSentenceRepo;

    @Override
    public Flux<Sentence> findByWord(Mono<String> find) {

        return reactiveSentenceRepo.findBySentenceContaining(find);
    }
}
