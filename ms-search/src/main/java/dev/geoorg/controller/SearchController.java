package dev.geoorg.controller;


import dev.geoorg.entity.Sentence;
import dev.geoorg.service.SentenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/client", produces = MediaType.APPLICATION_JSON_VALUE)
public class SearchController {

    private SentenceService sentenceService;

    @Autowired
    SearchController(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/find")
    public Flux<Sentence> find(@RequestParam String sentence) {
        Flux<Sentence> bySentence = sentenceService.findByWord(Mono.just(sentence));
        return bySentence;
    }
}
