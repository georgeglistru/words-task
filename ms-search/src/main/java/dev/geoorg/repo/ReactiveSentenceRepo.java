package dev.geoorg.repo;

import dev.geoorg.entity.Sentence;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface ReactiveSentenceRepo extends ReactiveCassandraRepository<Sentence, UUID> {

    @AllowFiltering
    Flux<Sentence> findBySentenceContaining(Mono<String> sentence);
}
