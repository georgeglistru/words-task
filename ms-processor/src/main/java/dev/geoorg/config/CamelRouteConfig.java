package dev.geoorg.config;

import org.apache.camel.builder.RouteBuilder;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CamelRouteConfig extends RouteBuilder {

    @Value("${properties.timer-delay}")
    private long delay;

    @Override
    public void configure() {

        final StringBuffer sentence = new StringBuffer();
        final MutableBoolean areWordsConsumed = new MutableBoolean(false);

        from("kafka:{{kafka.word-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}&autoCommitEnable={{kafka.auto-commit-enable}}")
                .process((exchange) -> {
                    synchronized (sentence) {
                        String word = exchange.getIn().getBody(String.class);
                        sentence.append(word).append(" ");
                    }
                })
                .onCompletion()
                .onWhen(exchange -> !areWordsConsumed.booleanValue())
                    .process((exchange -> areWordsConsumed.setTrue()))
                    .to("seda:delayer")
                    .end()
                .end()
                .to("mock:result");

        from("seda:delayer?concurrentConsumers=400")
                .delay(delay).asyncDelayed()
                .process(exchange -> {
                    exchange.getIn().setBody(sentence.substring(0, sentence.length() - 1), String.class);
                    sentence.setLength(0);
                    areWordsConsumed.setFalse();
                })
                .to("kafka:{{kafka.sentence-topic}}?brokers={{kafka.server}}:{{kafka.port}}");

    }

}