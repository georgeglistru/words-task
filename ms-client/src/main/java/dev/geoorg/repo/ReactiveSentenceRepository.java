package dev.geoorg.repo;


import dev.geoorg.entity.Sentence;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;

public interface ReactiveSentenceRepository extends ReactiveCassandraRepository<Sentence, Long> {

}
