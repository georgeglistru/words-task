package dev.geoorg.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Table("ms_sentence")
public class Sentence {

    public Sentence() {
        this.id = UUID.randomUUID();
    }

    public Sentence( String sentence) {
        this();
        this.sentence = sentence;
    }

    @PrimaryKey
    private UUID id;

    @NotNull
    private String sentence;

}