package dev.geoorg.service;

import dev.geoorg.dto.Word;
import dev.geoorg.entity.Sentence;
import dev.geoorg.repo.ReactiveSentenceRepository;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class WordServiceImpl implements WordService {

    @Produce("direct:wordSend")
    private ProducerTemplate template;

    private ReactiveSentenceRepository reactiveSentenceRepository;

    @Autowired
    public WordServiceImpl(ProducerTemplate template, ReactiveSentenceRepository reactiveSentenceRepository) {
        this.template = template;
        this.reactiveSentenceRepository = reactiveSentenceRepository;
    }

    @Override
    public Mono<Word> sendWord(Mono<Word> input) {
        input.subscribe(
                word -> template.asyncSendBody(template.getDefaultEndpoint(), word.getWord())
        );
        return input;
    }

    @Override
    public Mono<Sentence> saveSentence(String input) {
        Sentence sentence = new Sentence(input);
        final Mono<Sentence> save = reactiveSentenceRepository.save(sentence);
        save.subscribe();
        return save;
    }
    
}
