package dev.geoorg.service;

import dev.geoorg.dto.Word;
import dev.geoorg.entity.Sentence;
import reactor.core.publisher.Mono;

public interface WordService {

    /**
     * Method for sending word to Processor using Kafka
     * @param input - Sent word
     * @return Mono<Word>
     */
    Mono<Word> sendWord(Mono<Word> input);

    /**
     * Method for saving sentence in db
     * @param input saved sentence
     * @return Mono<Sentence>
     */
    Mono<Sentence> saveSentence(String input);

}
