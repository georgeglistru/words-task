package dev.geoorg.controller;

import dev.geoorg.dto.Word;
import dev.geoorg.service.WordService;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(path = "/client", produces = "application/json")
@CrossOrigin(origins = "*")
public class ClientController {

    private WordService wordService;

    @Autowired
    public ClientController(WordService wordService) {
        this.wordService = wordService;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/send", consumes = "application/json")
    public Mono<Word> send(@RequestBody Word word) {
        Mono<Word> wordMono = Mono.just(word);
        wordService.sendWord(wordMono);
        return wordMono;
    }
}
