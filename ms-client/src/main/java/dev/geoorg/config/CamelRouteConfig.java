package dev.geoorg.config;

import dev.geoorg.entity.Sentence;
import dev.geoorg.service.WordService;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CamelRouteConfig extends RouteBuilder {

    private WordService wordService;

    @Autowired
    public void setWordService(WordService wordService) {
        this.wordService = wordService;
    }

    @Override
    public void configure() {

        from("direct:wordSend")
                .to("kafka:{{kafka.word-topic}}?brokers={{kafka.server}}:{{kafka.port}}").end();

        from("kafka:{{kafka.sentence-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}&autoCommitEnable={{kafka.auto-commit-enable}}")
                .process((exchange) -> {
                    String sentence = exchange.getIn().getBody(String.class);
                    wordService.saveSentence(sentence);
                });

    }

}
